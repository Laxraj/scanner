//
//  RecentsScreen.swift
//  camscanner
//
//  Created by laxman on 18/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class RecentsScreen: UIViewController {
    
    @IBOutlet weak var recentTableView: UITableView!
    @IBOutlet weak var folderCollectionView: UICollectionView!
    @IBOutlet weak var folderCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recentDocumentTableViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDetail()
        
    }
    
    func initialDetail(){
        recentTableView.isHidden = false
        folderCollectionView.isHidden = true
        folderCollectionView.register(UINib(nibName: "FolderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "folderCell")
        recentTableView.register(UINib(nibName: "DocumentTableViewCell", bundle: nil), forCellReuseIdentifier:"DocumentCell")
    }
    
    @IBAction func onRecentScans(_ sender: UIButton) {
        if !sender.isSelected{
            recentTableView.isHidden = true
            sender.setImage(UIImage(named: "menu_icon"), for: .normal)
            sender.isSelected = true
        }else{
            recentTableView.isHidden = false
            sender.setImage(UIImage(named: "open_menu_icon"), for: .normal)
            sender.isSelected = false
        }
    }
    
    @IBAction func onAddNewFolder(_ sender: UIButton) {
        
    }
    
    @IBAction func onCreateFolder(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CreateNewFolderPopupScreen") as! CreateNewFolderPopupScreen
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func alertBox() {
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "Share", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
        }))
        
        alert.addAction(UIAlertAction(title: "Edit", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        }))
        
        alert.addAction(UIAlertAction(title: "Favourite", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}

extension RecentsScreen : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        if folderCollectionView == collectionView{
            DispatchQueue.main.async {
                self.folderCollectionViewHeight.constant = self.folderCollectionView.collectionViewLayout.collectionViewContentSize.height
                self.view.setNeedsLayout()
            }
        }
      }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = folderCollectionView.dequeueReusableCell(withReuseIdentifier: "folderCell", for: indexPath)as! FolderCollectionViewCell
            cell.folderIconImageView.image = UIImage(named: "yellow_folder_icon")
            cell.moreButton.addTarget(self, action: #selector(alertBox), for: .touchUpInside)
            return cell
     }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 120)
    }
}
extension RecentsScreen : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        recentDocumentTableViewHeight.constant = 61 * 20
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath)as! DocumentTableViewCell

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "PreviewDocumentScreen") as! PreviewDocumentScreen
        //        self.navigationController?.pushViewController(controller, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 61
    }

    
}




