//
//  GlobalConstant.swift
//  Iroid
//
//  Created by iroid on 30/03/18.
//  Copyright © 2018 iroidiroid. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


var currentLatitude = ""
var currentLongitude = ""
let POST_METHOD_CALL = true
let GET_METHOD_CALL = false
var player:AVPlayer?
var globalAlbumId = 0

var timer:Timer!
var musicUrlArray = NSArray()
var dataMutableDictionary = NSMutableDictionary()
var musicArrayIndex = 0
var globalDailyMotivationId = 0
let IN_APP_SENDBOX_URL = "https://sandbox.itunes.apple.com/verifyReceipt"
let IN_APP_LIVE_URL = "https://buy.itunes.apple.com/verifyReceipt"
let DEVICE_UNIQUE_IDETIFICATION : String = UIDevice.current.identifierForVendor!.uuidString
var userDetailsDictionar                 =      NSDictionary()
let TEST                                 =      "dnmdmnm"
let APPLICATION_NAME                     =      "CamScanner"
let DETECT_CATEGORY_CLICK                =      "detectCategoryClick"
let BASE_URL                             =      "https://docscanner.cf/"
let APLLICATION_JSON                     =      "application/json"
let API_ID_VALUE                         =      "f63ab8e4dc88fcda0826a2f695bfd7ba"
let API_SECRET_VALUE                     =      "1c4a417ce28bb18256ca150e4e8d3c6f"
let CART_PRODUCTS                        =      "cart_products"
let USER_DETAILS                         =      "user_details"
let musicPlayDataSave                    =      "musicPlayDataSave"
let PRODUCT_DETAILS                      =      "product_details"
let SUB_CATEGORY_DETAILS                 =      "sub_category_details"
let IS_LOGIN                             =      "is_login"
let ON_BOARDING                          =      "Onboarding"
let ON_BOARDING_PREMIUM                  =      "OnBoardingPremium"
let IS_GUEST_USER                        =      "is_guest_user"
let CATEGORY                             =      "category"
let ACCEPT                               =      "Accept"
let AccessTokenn                         =      "AccessTokenn"
let CITY_NAME                            =      "city"
let COUNTRY_NAME                         =      "country"
let KEYWORD                              =      "keyword"
let ACCESS_KEY                           =      "Bearer"
let ACCESS_TOKEN                         =      "access_token"
let AUTH                                 =      "auth"
let AUTHORIZATION                        =      "Authorization"
let DEVICE_TOKEN                         =      "DeviceToken"
let DEVICE_ID                            =      "deviceId"
let DEVICE_TYPE                          =      "DeviceType"
let SECRET                               =      "secret"
let NAME                                 =       "name"
let EMAIL                                =       "email"
let PASSWORD                             =      "password"
let CONFIRM_PASSWORD                     =      "c_password"
let PREVIOUS_PASSWORD                    =      "previous_password"
let SOCIAL_ID                            =      "social_id"



let CODE                                 =       "code"
let SOCIAL_provider                      =       "provider"


let SUCCESS                              =       "success"
let FLAG                                 =        "flag"
let MESSAGE                              =       "message"
let USERNAME                             =       "username"
let USER_IDD                             =       "userId"
let COMMAND                              =       "command"
let TAG                                  =       "tag"
let DATA                                 =       "data"
let REFRESH_TOKEN                        =         "refreshToken"
let REFRESSH_TOKENN                      =          "refresh_token"
let LOG_OUT_NOTIFICATION                 =         "LOG_OUT_NOTIFICATION"
let SERVER_DATE_FORMATE                  =       "yyyy-MM-dd HH:mm:ss"

//MARK: API
struct API
{
    static var REGISTER                  =          "api/register"
    static var LOGIN                     =          "api/login"
    static var UPDATE_EMAIL              =          "update/email"
    static var SOCIAL_LOGIN              =          "api/social"
    static var FORGOT_PASSWORD           =          "api/user/reset-password-email?"
    static var CHANGE_PASSWORD           =          "api/user/change-password"
    static var LOG_OUT                   =          "logout"
    static var CONTENT                   =          "content?type="
    static var HOME                      =          "home"
    static var CATEGORIES                =          "categories"
    static var EXPLORE                   =          "explore/"
    static var VERIFY_CODE               =          "send/verification"
    static var AUDIO_LIST                =          "audio/"
    static var LIBRARY                   =           "library?page="
    static var DELETE_LIBRARY            =           "library/"
    static var ADD_FAVOURITE             =           "favourite/"
    static var FAVOURITE_LIST            =           "favourite?page="
    static var SUBSCRIPTION              =           "subscription"
    static var REFRESH_TOKEN             =          "refresh/token"
    static var CHECL_APP_UPDATE          =          "check/update?v="
    
}

/*  old base url for DEV */
//let BASE_URL                             =      "http://13.233.208.36/bar-surge/api/v1/"





