//
//  OTPScreen.swift
//  camscanner
//
//  Created by laxman on 14/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class OTPScreen: UIViewController,UITextFieldDelegate {

    // MARK: - UITextField Outlet
       
       @IBOutlet weak var otpTextField1: UITextField!
       @IBOutlet weak var otpTextField2: UITextField!
       @IBOutlet weak var otpTextField3: UITextField!
       @IBOutlet weak var otpTextField4: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       initialDetail()
    }
    
    func initialDetail(){
        otpTextField1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpTextField1.addTarget(self, action: #selector(self.textField(_:shouldChangeCharactersIn:replacementString:)), for: UIControl.Event.editingChanged)
        otpTextField2.addTarget(self, action: #selector(self.textField(_:shouldChangeCharactersIn:replacementString:)), for: UIControl.Event.editingChanged)
        otpTextField3.addTarget(self, action: #selector(self.textField(_:shouldChangeCharactersIn:replacementString:)), for: UIControl.Event.editingChanged)
        otpTextField4.addTarget(self, action: #selector(self.textField(_:shouldChangeCharactersIn:replacementString:)), for: UIControl.Event.editingChanged)
        otpTextField1.becomeFirstResponder()
    }
    
    @IBAction func obBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Delegate Methods Of TextField
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case otpTextField1:
                otpTextField2.becomeFirstResponder()
            case otpTextField2:
                otpTextField3.becomeFirstResponder()
            case otpTextField3:
                otpTextField4.becomeFirstResponder()
            case otpTextField4:
                otpTextField4.resignFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case otpTextField1:
                otpTextField1.becomeFirstResponder()
            case otpTextField2:
                otpTextField1.becomeFirstResponder()
            case otpTextField3:
                otpTextField2.becomeFirstResponder()
            case otpTextField4:
                otpTextField3.becomeFirstResponder()
            default:
                break
            }
        }
        
    }
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             // figure out what the new string will be after the pending edit
             let sliced = String(textField.text!.dropFirst())
             if(textField.text!.count > 1){
                textField.text = sliced
             }
             return true
    }
}

