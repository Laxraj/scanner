//
//  ForgotPasswordScreem.swift
//  camscanner
//
//  Created by laxman on 15/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ForgotPasswordScreen: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onSendEmail(_ sender: UIButton) {
        doForgotPassword()
//        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "RecoverPasswordScreen") as! RecoverPasswordScreen
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func showSignUpAlert(msg:String) {
         let alert = UIAlertController(title: APPLICATION_NAME, message: msg , preferredStyle: UIAlertController.Style.alert)
         
         alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
             self.navigationController?.popViewController(animated: true)
         }))
         
         self.present(alert, animated: true, completion: nil)
     }
  
}
//MARK: Api calle
@available(iOS 13.0, *)
extension ForgotPasswordScreen : RequestManagerDelegate{
    
    func doForgotPassword(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let request = RequestManager()
            request.delegate = self
            request.commandName = API.FORGOT_PASSWORD
             let parameters = [EMAIL:emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)]
            request.CallGetURL(url: BASE_URL, parameters: parameters as NSDictionary)
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func onResult(result: NSMutableDictionary) {
        Utility.hideIndicator()
        if result[COMMAND] as! String == API.FORGOT_PASSWORD{
            if result[SUCCESS]! as! NSNumber as! Bool{
               showSignUpAlert(msg: (result[MESSAGE] as? String)!)
               
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }else if result[COMMAND] as! String == API.SOCIAL_LOGIN{
            if result[SUCCESS]! as! NSNumber as! Bool{
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin: result[DATA]! as! [String : AnyObject])
                let AccessToken =   dataResponseDictionary[AUTH] as! NSDictionary
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(AccessToken, forKey:AccessTokenn)
                //               if dataResponseDictionary[IS_SUBSCRIPTION_RUNNING] as? Int == 1{
                //                    let storyBoard = UIStoryboard(name: "TabBar", bundle: nil)
                //                    let control = storyBoard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                //                    self.navigationController?.pushViewController(control, animated: true)
                //                }else{
                //                    let storyBoard = UIStoryboard(name: "ChoosePlan", bundle: nil)
                //                    let control = storyBoard.instantiateViewController(withIdentifier: "ChoosePlanScreen") as! ChoosePlanScreen
                //                    control.checkNavigation = true
                //                    self.navigationController?.pushViewController(control, animated: true)
                //                }
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }
    }
    
    func onFault(error: NSError) {
        Utility.hideIndicator()
        Utility.showAlert(vc: self,message:error.localizedDescription)
        print(error)
    }
}
