//
//  SignUpScreen.swift
//  camscanner
//
//  Created by laxman on 15/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SignUpScreen: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    //MARK: social login variable
    var socialDictionary : [String : AnyObject]!
    var socialEmailId:String!
    var socialName:String!
    var socialId:String!
    var socialPic = ""
    var socialType:String!
    var socialGender = ""
    var socialBirthdate = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onSignUp(_ sender: UIButton) {
        if nameTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter full name")
            return
        }else if emailTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter your email")
            return
        }else if !emailTextField.text.isEmailValid(){
            Utility.showAlert(vc: self, message: "Please enter a valid email")
            return
        }else if passwordTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter a password")
            return
        }else if confirmPasswordTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter a confirm password")
            return
        }else if passwordTextField.text != confirmPasswordTextField.text{
            Utility.showAlert(vc: self, message: "password are not match")
            return
        }else{
            doSignUp()
        }
        
        //        let storyboard = UIStoryboard(name: "OTP", bundle: nil)
        //               let viewController = storyboard.instantiateViewController(withIdentifier: "OTPScreen") as! OTPScreen
        //               self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func onTermsOfUse(_ sender: UIButton) {
    }
    @IBAction func onPrivacyPolicy(_ sender: UIButton) {
    }
    
    func showSignUpAlert(msg:String) {
        let alert = UIAlertController(title: APPLICATION_NAME, message: msg , preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
//MARK: Api calle
@available(iOS 13.0, *)
extension SignUpScreen : RequestManagerDelegate{
    
    func doSignUp(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let request = RequestManager()
            request.delegate = self
            request.commandName = API.REGISTER
            
            let parameters = [NAME:nameTextField.text!,
                              EMAIL:emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                              PASSWORD:passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                              CONFIRM_PASSWORD:confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                
            ]
            request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func doSocialLogin()
    {
        //        if(Utility.isInternetAvailable()){
        //            Utility.showIndicator()
        //            let request = RequestManager()
        //            request.delegate = self
        //            let parameters = [EMAIL:socialEmailId, USERNAME:socialName,SOCIAL_ID:socialId,PROFILE:socialPic,SOCIAL_provider:socialType,TIMEZONE:"",MOBILE:""]
        //            request.commandName = API.SOCIAL_LOGIN
        //            request.delegate = self
        //            request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
        //        }
        //        else
        //        {
        //            Utility.showNoInternetConnectionAlertDialog(vc: self)
        //        }
    }
    func onResult(result: NSMutableDictionary) {
        Utility.hideIndicator()
        if result[COMMAND] as! String == API.REGISTER{
            
            print(result)
            if result[SUCCESS]! as! NSNumber as! Bool{
                //                showSignUpAlert(msg: (result[MESSAGE] as? String)!)
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin:  result[DATA] as! [String : AnyObject])
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(dataResponseDictionary[ACCESS_TOKEN] as! String, forKey:AccessTokenn)
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }else if result[COMMAND] as! String == API.SOCIAL_LOGIN{
            if result[SUCCESS]! as! NSNumber as! Bool{
                
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin: result[DATA]! as! [String : AnyObject])
                let AccessToken =   dataResponseDictionary[AUTH] as! NSDictionary
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(AccessToken, forKey:AccessTokenn)
                
                
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }
    }
    
    func onFault(error: NSError) {
        Utility.showIndicator()
        print(error)
    }
}
