//
//  RecoverPasswordScreen.swift
//  camscanner
//
//  Created by laxman on 15/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class RecoverPasswordScreen: UIViewController {
    
    
    @IBOutlet weak var currentPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onChangePassword(_ sender: UIButton) {
        if currentPassword.text == ""{
            Utility.showAlert(vc: self, message: "Please current password")
            return
        }else if newPassword.text == ""{
            Utility.showAlert(vc: self, message: "Please new password")
            return
        }else if confirmPassword.text == ""{
            Utility.showAlert(vc: self, message: "Please enter confirm password")
            return
        }else if newPassword.text != confirmPassword.text{
            Utility.showAlert(vc: self, message: "password are not match")
            return
        }
        
        doChangePassword()
    }
    func showSignUpAlert(msg:String) {
        let alert = UIAlertController(title: APPLICATION_NAME, message: msg , preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
//MARK: Api calle
@available(iOS 13.0, *)
extension RecoverPasswordScreen : RequestManagerDelegate{
    
    func doChangePassword(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let request = RequestManager()
            request.delegate = self
            request.commandName = API.CHANGE_PASSWORD
            
            let parameters = [PREVIOUS_PASSWORD:currentPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                              CONFIRM_PASSWORD:newPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                              PASSWORD:confirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            ]
            request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func onResult(result: NSMutableDictionary) {
        Utility.hideIndicator()
        if result[COMMAND] as! String == API.CHANGE_PASSWORD{
            if result[SUCCESS]! as! NSNumber as! Bool{
//                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
                
                let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
                               let viewController = storyboard.instantiateViewController(withIdentifier:"WellDoneScreen") as! WellDoneScreen
                               self.navigationController?.pushViewController(viewController, animated: true)
                
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }
    }
    
    func onFault(error: NSError) {
        Utility.showIndicator()
        print(error)
    }
}
