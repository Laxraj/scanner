//
//  WelcomeScreen.swift
//  camscanner
//
//  Created by laxman on 15/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit
import GoogleSignIn
@available(iOS 13.0, *)
class WelcomeScreen: UIViewController {

    //MARK: social login variable
    var socialDictionary : [String : AnyObject]!
    var socialEmailId:String!
    var socialName:String!
    var socialId:String!
    var socialType:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onEmailLogIn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpScreen") as! SignUpScreen
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func onAppleLogIn(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleLoginManager = AppleLoginManager()
            appleLoginManager.delegate = self
            appleLoginManager.mainView = self.view
            appleLoginManager.handleAuthorizationAppleIDButtonPress()
        } else {
        }
    }
    
    @IBAction func onGoogleLogIn(_ sender: Any) {
        let googleManager = GoogleLoginManager()
        googleManager.delegate = self
        googleManager.handleGoogleLoginButtonTap(viewController: self)
    }
  
    @IBAction func onLogIn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
  
}
@available(iOS 13.0, *)
extension WelcomeScreen : RequestManagerDelegate{
    func doSocialLogin()
        {
            if(Utility.isInternetAvailable()){
                Utility.showIndicator()
                let request = RequestManager()
                request.delegate = self
                let parameters = [EMAIL:socialEmailId, USERNAME:socialName,SOCIAL_ID:socialId,SOCIAL_provider:socialType]
                request.commandName = API.SOCIAL_LOGIN
                request.delegate = self
                request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
            }
            else
            {
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
    
    func onResult(result: NSMutableDictionary) {
        Utility.hideIndicator()
        if result[COMMAND] as! String == API.SOCIAL_LOGIN{
            if result[SUCCESS]! as! NSNumber as! Bool{
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin:  result[DATA] as! [String : AnyObject])
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(dataResponseDictionary[ACCESS_TOKEN] as! String, forKey:AccessTokenn)
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }
    }
      
      func onFault(error: NSError) {
          Utility.showIndicator()
          print(error)
      }
}
@available(iOS 13.0, *)
extension WelcomeScreen: googleLoginManagerDelegate{
    func onGoogleLoginSuccess(user: GIDGoogleUser) {
        print(user)
        socialId = user.userID                  // For client-side use only!
        //        let idToken = user.authentication.idToken // Safe to send to the server
        socialName = user.profile.name + "\(user.profile.givenName ?? "")"
        socialEmailId = user.profile.email
        socialType = "google"
        doSocialLogin()
    }
    func onGoogleLoginFailure(error: NSError) {
        print(error.description)
        
    }
}
//MARK: AppleLoginManagerDelegate
@available(iOS 13.0, *)
extension WelcomeScreen:AppleLoginManagerDelegate{
    func onSuccess(result: NSDictionary) {
        print(result)
        socialId = result[USER_IDD] as? String
        socialName = result[USERNAME] as? String
        socialEmailId = result[EMAIL] as? String
        socialType = result[SOCIAL_provider] as? String
        doSocialLogin()
    }
    
    func onFailure(error: NSError) {
        print(error.description)
    }
}
