//
//  LoginScreen.swift
//  camscanner
//
//  Created by laxman on 15/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit
import GoogleSignIn

@available(iOS 13.0, *)
class LoginScreen: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: social login variable
    var socialDictionary : [String : AnyObject]!
    var socialEmailId:String!
    var socialName:String!
    var socialId:String!
    var socialPic = ""
    var socialType:String!
    var socialGender = ""
    var socialBirthdate = ""
    var statusCodeViewController = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onSignUp(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpScreen") as! SignUpScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func onForget(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordScreen") as! ForgotPasswordScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func onLogin(_ sender: UIButton) {
        if emailTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter your email")
            return
        }else if !emailTextField.text.isEmailValid(){
            Utility.showAlert(vc: self, message: "Please enter a valid email")
            return
        }else if passwordTextField.text == ""{
            Utility.showAlert(vc: self, message: "Please enter a password")
            return
        }else{
            doLogin()
        }
    }
    
    @IBAction func onGoogle(_ sender: UIButton) {
        let googleManager = GoogleLoginManager()
        googleManager.delegate = self
        googleManager.handleGoogleLoginButtonTap(viewController: self)
    }
    
    @IBAction func onAppleLogin(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            let appleLoginManager = AppleLoginManager()
            appleLoginManager.delegate = self
            appleLoginManager.mainView = self.view
            appleLoginManager.handleAuthorizationAppleIDButtonPress()
        } else {
            // Fallback on earlier versions
        }
    }
}
//MARK: Api calle
@available(iOS 13.0, *)
extension LoginScreen : RequestManagerDelegate{
    
    func doLogin(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let request = RequestManager()
            request.delegate = self
            request.commandName = API.LOGIN
            
            let parameters = [EMAIL:emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                              PASSWORD:passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            ]
            request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func doSocialLogin()
    {
        if(Utility.isInternetAvailable()){
            Utility.showIndicator()
            let request = RequestManager()
            request.delegate = self
            let parameters = [EMAIL:socialEmailId, USERNAME:socialName,SOCIAL_ID:socialId,SOCIAL_provider:socialType]
            request.commandName = API.SOCIAL_LOGIN
            request.delegate = self
            request.CallPostURL(url: BASE_URL, parameters: parameters as NSDictionary)
        }
        else
        {
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func onResult(result: NSMutableDictionary) {
        Utility.hideIndicator()
        if result[COMMAND] as! String == API.LOGIN{
            if result[SUCCESS]! as! NSNumber as! Bool{
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin:  result[DATA] as! [String : AnyObject])
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(dataResponseDictionary[ACCESS_TOKEN] as! String, forKey:AccessTokenn)
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }else if result[COMMAND] as! String == API.SOCIAL_LOGIN{
            if result[SUCCESS]! as! NSNumber as! Bool{
                let dataResponseDictionary = Utility.removeNullsFromDictionary(origin:  result[DATA] as! [String : AnyObject])
                UserDefaults().set(dataResponseDictionary, forKey: USER_DETAILS)
                UserDefaults().set("1", forKey: IS_LOGIN)
                UserDefaults().set(dataResponseDictionary[ACCESS_TOKEN] as! String, forKey:AccessTokenn)
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                Utility.showAlert(vc: self, message: (result[MESSAGE] as? String)!)
            }
        }
    }
    
    func onFault(error: NSError) {
        Utility.showIndicator()
        print(error)
    }
}
@available(iOS 13.0, *)
extension LoginScreen: googleLoginManagerDelegate{
    func onGoogleLoginSuccess(user: GIDGoogleUser) {
        print(user)
        socialId = user.userID                  // For client-side use only!
        //        let idToken = user.authentication.idToken // Safe to send to the server
        socialName = user.profile.name + "\(user.profile.givenName ?? "")"
        socialEmailId = user.profile.email
        socialPic = user.profile.imageURL(withDimension: 200)!.absoluteString
        socialType = "google"
        doSocialLogin()
    }
    func onGoogleLoginFailure(error: NSError) {
        print(error.description)
        
    }
}
//MARK: AppleLoginManagerDelegate
@available(iOS 13.0, *)
extension LoginScreen:AppleLoginManagerDelegate{
    func onSuccess(result: NSDictionary) {
        print(result)
        socialId = result[USER_IDD] as? String
        socialName = result[USERNAME] as? String
        socialEmailId = result[EMAIL] as? String
        socialPic = ""
        socialType = result[SOCIAL_provider] as? String
        doSocialLogin()
    }
    
    func onFailure(error: NSError) {
        print(error.description)
    }
}
