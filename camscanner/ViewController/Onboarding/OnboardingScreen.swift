//
//  OnboardingScreen.swift
//  camscanner
//
//  Created by laxman on 14/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class OnboardingScreen: UIViewController {
    
    
    @IBOutlet weak var onBoardingCollectionView: UICollectionView!
    @IBOutlet weak var pageControllerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialDetail()
    }
    
    func initialDetail(){
        backButton.isHidden = true
        pageControllerImageView?.image = UIImage(named: "first_page_control")
    }
    
    func mainScreenNavigation(){
        
        let storyboard = UIStoryboard(name: "MainOption", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainOptionScreen") as! MainOptionScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        let visibleItems: NSArray = self.onBoardingCollectionView.indexPathsForVisibleItems as NSArray
             let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
             let nextItem: NSIndexPath = NSIndexPath(row: currentItem.row - 1, section: 0)
             
             
             self.onBoardingCollectionView.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
    }
    @IBAction func onNext(_ sender: UIButton) {
        let visibleItems: NSArray = self.onBoardingCollectionView.indexPathsForVisibleItems as NSArray
        let currentItem: NSIndexPath = visibleItems.object(at: 0) as! NSIndexPath
        
        if currentItem.row == 3{
            mainScreenNavigation()
            UserDefaults().set("1", forKey: ON_BOARDING)
            return
        }
        let nextItem: NSIndexPath = NSIndexPath(row: currentItem.row + 1, section: 0)
        self.onBoardingCollectionView.scrollToItem(at: nextItem as IndexPath, at: .left, animated: true)
    }
    
    
    @IBAction func onSkip(_ sender: UIButton) {
         UserDefaults().set("1", forKey: ON_BOARDING)
        mainScreenNavigation()
    }
    
}
@available(iOS 13.0, *)
extension OnboardingScreen:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "introductionCell", for: indexPath)
        let introImageView = cell.viewWithTag(100) as? UIImageView
//        let titleLabel = cell.viewWithTag(101) as? UILabel
//        let discriptionLabel = cell.viewWithTag(102) as? UILabel
        
        if indexPath.row == 0 {
            introImageView?.image = UIImage(named: "illustration_page1_editable_text_from_image")
//            titleLabel?.text = "Editable text from image"
//            discriptionLabel?.text = "Save document to wide selection of cloud "
            
        }else if indexPath.row == 1 {
            introImageView?.image = UIImage(named: "illustration_p2_save_document_to_cloud")
//            titleLabel?.text = "Save document to cloud"
//
//            discriptionLabel?.text = "Save document to wide selection of cloud drives"
            
        }else if indexPath.row == 2 {
            introImageView?.image = UIImage(named: "illustration_p3_add_signature_and_watermark")
//            titleLabel?.text = "Add signature"
//            discriptionLabel?.text = "Add your personal signature just with one tap"
            
        }else if indexPath.row == 3 {
            introImageView?.image = UIImage(named: "illustration_p4_share_to_multiple_platforms")
//            titleLabel?.text = "Share to multiple platforms"
//            discriptionLabel?.text = "Share document to wide selection of platforms"
            
        }
        return cell
    }
}
@available(iOS 13.0, *)
extension OnboardingScreen:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let index = onBoardingCollectionView.indexPathForItem(at: center) {
            if index.row == 0{
                pageControllerImageView?.image = UIImage(named: "first_page_control")
                titleLabel.text = "Editable text from image"
                descriptionLabel.text = "Scan and change image document to word file instantly with sophisticated OCR. Save, edit and share."
                backButton.isHidden = true
                nextButton.setTitle("Next", for: .normal)
            }else if index.row == 1{
                pageControllerImageView?.image = UIImage(named: "second_page_control")
                titleLabel.text = "Save document to cloud"
                               descriptionLabel.text = "Save document to wide selection of cloud like Dropbox, Evernote, Drive, Box, Onedrive, Files"
                nextButton.setTitle("Next", for: .normal)
                 backButton.isHidden = false
            }else if index.row == 2{
                pageControllerImageView?.image = UIImage(named: "third_page_control")
                titleLabel.text = "Add signature and watermark"
                descriptionLabel.text = "Add your personal signature with one tap on one or many files. Add personalize signature for protection."
               nextButton.setTitle("Next", for: .normal)
                backButton.isHidden = false
            }else{
                pageControllerImageView?.image = UIImage(named: "forth_page_control")
                titleLabel.text = "Share to multiple platforms"
                descriptionLabel.text = "Share document to wide selection of platforms like Dropbox, Evernote, Drive, Box, Onedrive, Files, icloud, print, photos, fax"
                nextButton.setTitle("Get Started", for: .normal)
                backButton.isHidden = false
            }
        }
    }
}
@available(iOS 13.0, *)
extension OnboardingScreen: UICollectionViewDelegateFlowLayout{
    
   func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: collectionView.frame.size.height)
    }
}
