//
//  FolderCollectionViewCell.swift
//  camscanner
//
//  Created by Kishan on 16/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class FolderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var folderIconImageView: UIImageView!
    
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var containerDataLabel: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
