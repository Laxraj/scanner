//
//  MoreOptionsTableViewCell.swift
//  camscanner
//
//  Created by Kishan on 18/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class MoreOptionsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var docInfoView: UIView!
    @IBOutlet weak var optionsView: UIView!
    
    @IBOutlet weak var optionsLabel: UILabel!
    @IBOutlet weak var folderInfoLabel: UILabel!
    @IBOutlet weak var folderNameLabel: UILabel!
    
    @IBOutlet weak var folderImageView: UIImageView!
    @IBOutlet weak var optionsImageView: UIImageView!
    @IBOutlet weak var optionBgImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
