//
//  DocumentScreen.swift
//  DocumentScannerApp
//
//  Created by laxman on 02/07/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit
import DropDown
class DocumentScreen: UIViewController  {
    
    
    @IBOutlet weak var recentTableView: UITableView!
    @IBOutlet weak var recentCollectionView: UICollectionView!
    @IBOutlet weak var folderCollectionView: UICollectionView!
    @IBOutlet weak var folderCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recentCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var moreOptionButton: UIButton!
    @IBOutlet weak var recentButton: UIButton!
    
    
    @IBOutlet weak var chooseArticleButton: UIButton!
    @IBOutlet weak var amountButton: UIButton!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var centeredDropDownButton: UIButton!
    
    
    //MARK: - DropDown's
    let centeredDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.centeredDropDown
        ]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialDetail()
        setupDropDowns()
//        dropDowns.forEach { $0.dismissMode = .onTap }
//        dropDowns.forEach { $0.direction = .any }
        customizeDropDown(self)
      
        folderCollectionView.register(UINib(nibName: "FolderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "folderCell")
        recentCollectionView.register(UINib(nibName: "FolderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "folderCell")
    }
    
    func setupDropDowns() {
        setupCenteredDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func initialDetail(){
        recentTableView.isHidden = true
    }
    
    
    @IBAction func onRecentScans(_ sender: UIButton) {
        if !sender.isSelected{
            recentTableView.isHidden = true
            recentCollectionView.isHidden = false
            sender.setImage(UIImage(named: "menu_icon"), for: .normal)
            sender.isSelected = true
        }else{
            recentTableView.isHidden = false
            recentCollectionView.isHidden = true
            sender.setImage(UIImage(named: "open_menu_icon"), for: .normal)
            sender.isSelected = false
        }
    }
  
    @IBAction func showCenteredDropDown(_ sender: AnyObject) {
       // centeredDropDown.show()
    }
    
      
    
    @IBAction func onMoreOption(_ sender: UIButton) {
        
    }
    
    @IBAction func onAddNewFolder(_ sender: UIButton) {
        
    }
    
    @IBAction func onCreateFolder(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CreateNewFolderPopupScreen") as! CreateNewFolderPopupScreen
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func alertBox() {
        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OptionsScreen") as! OptionsScreen
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true, completion: nil)
    }
    @objc func recentAlertBox(){
        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "OptionsScreen") as! OptionsScreen
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true, completion: nil)
    }
    
      func setupCenteredDropDown() {
                
                centeredDropDown.dataSource = [
                    "Select",
                    "New Folder",
                    "List",
                    "Name",
                    "Data",
                    "Size"
                ]
                
                centeredDropDown.selectionAction = { [weak self] (index, item) in
                    self?.centeredDropDownButton.setTitle(item, for: .normal)
                }
            }
      
            func customizeDropDown(_ sender: AnyObject) {
                let appearance = DropDown.appearance()
                
                appearance.cellHeight = 200
                
                appearance.backgroundColor = UIColor(white: 1, alpha: 1)
                appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        //        appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
    //            appearance.cornerRadius = 10
    //            appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
    //            appearance.shadowOpacity = 0.9
    //            appearance.shadowRadius = 25
    //            appearance.animationduration = 0.25
                appearance.textColor = .darkGray
        //        appearance.textFont = UIFont(name: "Georgia", size: 14)

                if #available(iOS 11.0, *) {
                    appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
                }
                
                dropDowns.forEach {
                    /*** FOR CUSTOM CELLS ***/
                    $0.cellNib = UINib(nibName: "MyCell", bundle: nil)
                    
                    $0.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
                        guard let cell = cell as? MyCell else { return }
                        
                        // Setup your custom UI components
                        cell.logoImageView.image = UIImage(named: "logo_\(index % 10)")
                    }
                    /*** ---------------- ***/
                }
            }
}

extension DocumentScreen : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if folderCollectionView == collectionView{
            return 20
        }
        else{
            return 20
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        if folderCollectionView == collectionView{
            DispatchQueue.main.async {
                self.folderCollectionViewHeight.constant = self.folderCollectionView.collectionViewLayout.collectionViewContentSize.height
                self.view.setNeedsLayout()
            }
        }
        else{
            DispatchQueue.main.async {
                self.recentCollectionViewHeight.constant = self.recentCollectionView.collectionViewLayout.collectionViewContentSize.height
                self.view.setNeedsLayout()
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if folderCollectionView == collectionView{
            let cell = folderCollectionView.dequeueReusableCell(withReuseIdentifier: "folderCell", for: indexPath)as! FolderCollectionViewCell
            cell.folderIconImageView.image = UIImage(named: "yellow_folder_icon")
            cell.moreButton.addTarget(self, action: #selector(alertBox), for: .touchUpInside)
            return cell
        }
        else{
            let cell = recentCollectionView.dequeueReusableCell(withReuseIdentifier: "folderCell", for: indexPath)as! FolderCollectionViewCell
            cell.folderIconImageView.image = UIImage(named: "jpg_icon")
            cell.moreButton.addTarget(self, action: #selector(recentAlertBox), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfCellsInRow = 2
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 120)
    }
}
extension DocumentScreen : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recentScanTableViewCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let storyboard = UIStoryboard(name: "Document", bundle: nil)
        //        let controller = storyboard.instantiateViewController(withIdentifier: "PreviewDocumentScreen") as! PreviewDocumentScreen
        //        self.navigationController?.pushViewController(controller, animated: true)
    }
}

