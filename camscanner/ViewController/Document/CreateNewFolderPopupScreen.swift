//
//  Create New Folder Popup ScreenViewController.swift
//  camscanner
//
//  Created by Kishan on 16/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class CreateNewFolderPopupScreen: UIViewController {

    @IBOutlet weak var permissionLabel: UILabel!
    @IBOutlet weak var unlockFolderLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
    var isForEdit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetails()
        // Do any additional setup after loading the view.
    }
    
    func initialDetails() {
        if isForEdit{
            isForEdit = false
            permissionLabel.text = "Lock Folder "
            unlockFolderLabel.text = "You can change your MPIN or fingerprint password of your Folder from Setting under Account"
            colorLabel.text = "Select Folder Color"
        }
        else{
            permissionLabel.text = "Lock Folder Premium Feature"
            unlockFolderLabel.text = "Unlock Folder lock features with premium features along with features like OCR and Signature"
            colorLabel.text = "Change Folder Color"
            permissionLabel.halfTextColorChange(fullText: permissionLabel.text!, changeText: "Premium Feature")
            unlockFolderLabel.halfTextColorChange(fullText: unlockFolderLabel.text!, changeText: "Folder lock")
        }
        
    }

   
    @IBAction func onCancel(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onCreate(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}



extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        let TextColor = UIColor(red: 51/255, green: 102/255, blue: 255/255, alpha: 1.0)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: TextColor , range: range)
        self.attributedText = attribute
    }
}
