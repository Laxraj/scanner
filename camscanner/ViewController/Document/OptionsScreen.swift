//
//  OptionsScreen.swift
//  camscanner
//
//  Created by Kishan on 18/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

class OptionsScreen: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var blurView: UIView!
    
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var optionTableView: UITableView!
    
    var optionsLabelArray = NSMutableArray()
    var optionsImagesArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetails()
        optionTableView.register(UINib(nibName: "MoreOptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "optionCell")
        let tap = UITapGestureRecognizer(target: self, action: #selector(onDismiss))
        tap.delegate = self
        blurView.addGestureRecognizer(tap)
    }
    
    func initialDetails() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = optionView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.layer.cornerRadius = 10
       // blurEffectView.cornerRadius = 10
        optionView.insertSubview(blurEffectView, at: 0)
        optionsLabelArray = ["Share", "Access Control","Export","Edit","Move","Copy","Remove"]
        optionsImagesArray = ["Share", "Access Control","Export","Rename","Move","Copy","Remove"]
        optionTableView.reloadData()
    }
    
    @objc func onDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension OptionsScreen : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsLabelArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = optionTableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)as! MoreOptionsTableViewCell
        if indexPath.row == 0 {
            cell.optionsView.isHidden = true
            cell.docInfoView.isHidden = false
        }
        else{
            cell.optionsView.isHidden = false
            cell.docInfoView.isHidden = true
            cell.optionsLabel.text = (optionsLabelArray.object(at: indexPath.row - 1)as! String)
            cell.optionsImageView.image = UIImage(named: optionsImagesArray.object(at: indexPath.row - 1)as! String)
        }
        if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6 {
            cell.optionBgImageView.isHidden = false
        }
        else{
            cell.optionBgImageView.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = (optionsLabelArray.object(at: indexPath.row - 1)as! String)
        if option == "Edit" {
            weak var pr = presentingViewController
            self.dismiss(animated: true, completion: {
                let storyboard = UIStoryboard(name: "Document", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "CreateNewFolderPopupScreen") as! CreateNewFolderPopupScreen
                viewController.isForEdit = true
                viewController.modalPresentationStyle = .overFullScreen
                pr!.present(viewController, animated: true, completion: nil)
            })
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
