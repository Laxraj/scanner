//
//  TabbarScreenViewController.swift
//  DocumentScannerApp
//
//  Created by laxman on 02/07/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit
import JJFloatingActionButton
class TabBarScreen: UITabBarController {
    
    var isFirstTime:Bool = true
    var centerButton = UIButton()
    let actionButton = JJFloatingActionButton()
    
    //MARK:-- ImagePicker For Pick Images
    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       setupCenterButton()
        initialDetail()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func setupCenterButton() {
        view.addSubview(actionButton)
        
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -UIScreen.main.bounds.size.width/2+27).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: tabBar.safeAreaLayoutGuide.topAnchor, constant: 56).isActive = true
        
        actionButton.handleSingleActionDirectly = false
        actionButton.buttonDiameter = 54
        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        actionButton.buttonImage = UIImage(named: "Dots")
        actionButton.buttonColor = #colorLiteral(red: 0.01568627451, green: 0.3921568627, blue: 0.9176470588, alpha: 1)
        actionButton.buttonImageColor = .white
        actionButton.buttonImageSize = CGSize(width: 30, height: 30)
        
        
        actionButton.buttonAnimationConfiguration = .transition(toImage: #imageLiteral(resourceName: "cancel_icon"))
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        
        actionButton.layer.shadowColor = #colorLiteral(red: 0.01568627451, green: 0.3921568627, blue: 0.9176470588, alpha: 0.3469071062)
        actionButton.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        actionButton.layer.shadowOpacity = Float(1)
        actionButton.layer.shadowRadius = CGFloat(3)
        
        actionButton.itemSizeRatio = CGFloat(0.75)
        actionButton.configureDefaultItem { item in
            item.titlePosition = .trailing
            
            item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
            item.titleLabel.textColor = .white
            item.buttonColor = #colorLiteral(red: 0.01568627451, green: 0.3921568627, blue: 0.9176470588, alpha: 1)
            item.buttonImageColor = .red
            
            item.layer.shadowColor = UIColor.black.cgColor
            item.layer.shadowOffset = CGSize(width: 0, height: 1)
            item.layer.shadowOpacity = Float(0.4)
            item.layer.shadowRadius = CGFloat(2)
        }
        
        addItems()
    }
    @objc private func centerButtonAction(sender: UIButton) {
        selectedIndex = 1
    }
    
    
    func initialDetail(){
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.5764705882, green: 0.5764705882, blue: 0.6666666667, alpha: 1)
        setTabBarControllers()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideTabBarView(_:)), name: NSNotification.Name(rawValue: "Hide_Camera_button"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showTabBarView(_:)), name: NSNotification.Name(rawValue: "Show_Camera_button"), object: nil)
        
    }
    
    fileprivate func addItems() {
        actionButton.addItem(title: "", image: UIImage(named: "camera_icon")) { item in
            self.openCamera()
        }
        
        actionButton.addItem(title: "", image: UIImage(named: "gallery_icon")) { item in
            self.openGallery()
        }
        actionButton.addItem(title: "", image: UIImage(named: "OCR_icon")) { item in
            self.openScanner()
        }
    }
    
    
    
    @objc func showTabBarView(_ notification: NSNotification){
        
        centerButton.isHidden = false
        self.tabBarController?.tabBar.layer.zPosition = 0
        centerButton.layer.zPosition = 1
        
        
    }
    
    @objc func hideTabBarView(_ notification: NSNotification){
        centerButton.isHidden = true
    }
    
    
    
    func setTabBarControllers() {
        //self.tabBar.barTintColor = UIColor.white //white
        self.navigationController?.isNavigationBarHidden = true
        
        
        let RecentsStoryboard = UIStoryboard(name: "Recents", bundle: nil)
        let RecentsScreen = RecentsStoryboard.instantiateViewController(withIdentifier: "RecentsScreen") as! RecentsScreen
        
        let CameraStoryboard = UIStoryboard(name: "Camera", bundle: nil)
        let CameraScreen = CameraStoryboard.instantiateViewController(withIdentifier: "CameraScreen") as! CameraScreen
        
        let DocumentStoryboard = UIStoryboard(name: "Document", bundle: nil)
        let DocumentScreen = DocumentStoryboard.instantiateViewController(withIdentifier: "DocumentScreen") as! DocumentScreen
        
        
        
        let recentsNavigationController = UINavigationController(rootViewController:RecentsScreen)
        let cameraNavigationController = UINavigationController(rootViewController: CameraScreen)
        let documentNavigationController = UINavigationController(rootViewController: DocumentScreen)
        
        
        recentsNavigationController.isNavigationBarHidden = true
        cameraNavigationController.isNavigationBarHidden = true
         documentNavigationController.isNavigationBarHidden = true
        
        
        viewControllers = [recentsNavigationController, cameraNavigationController, documentNavigationController]
        
        self.tabBar.items![0].image = UIImage(named: "scan_document_icon")
        self.tabBar.items![1].image = nil
        self.tabBar.items![2].image = UIImage(named: "Browse_icon")
        
        self.tabBar.items?[0].title = "Recent"
        self.tabBar.items?[2].title = "Browse"
        selectedViewController = viewControllers![0]
        
        
    }
    
    
}

extension TabBarScreen: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    //MARK:-- Open Camera
       func openCamera()
       {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
           {
               imagePicker.sourceType = UIImagePickerController.SourceType.camera
               imagePicker.allowsEditing = true
               imagePicker.delegate = self
               self.present(imagePicker, animated: true, completion: nil)
           }
           else
           {
               let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
               self.present(alert, animated: true, completion: nil)
           }
       }
    
    //MARK:-- Open Scanner
    func openScanner(){
       let storyBoard = UIStoryboard(name: "Scan", bundle: nil)
        let control = storyBoard.instantiateViewController(withIdentifier: "ScannerScreen") as! ScannerScreen
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    //MARK:-- Open Gallery
       func openGallery()
       {
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
           imagePicker.allowsEditing = true
           imagePicker.delegate = self
           self.present(imagePicker, animated: true, completion: nil)
       }
    
    //MARK:-- ImagePicker delegate
      func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if let pickedImage = info[.originalImage] as? UIImage {
             // profileImageView.image = pickedImage
          }
          picker.dismiss(animated: true, completion: nil)
      }
    
}
