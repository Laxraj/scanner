//
//  MainOptionScreen.swift
//  camscanner
//
//  Created by laxman on 14/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class MainOptionScreen: UIViewController {
    
    @IBOutlet weak var mainOptionCollectionView: UICollectionView!
    @IBOutlet weak var pageControllerImageView: UIImageView!
    
    @IBOutlet weak var continueLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var yearlyImage: UIImageView!
    @IBOutlet weak var monthlyImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialDetail()
    }
    
    func initialDetail(){
        pageControllerImageView?.image = UIImage(named: "first_white_page_control")
    }
    
    @IBAction func onLogin(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func onSignUp(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SignUpScreen") as! SignUpScreen
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func onSkip(_ sender: UIButton) {
        UserDefaults().set("1", forKey: ON_BOARDING_PREMIUM)
        let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "WelcomeScreen") as! WelcomeScreen
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func onYearly(_ sender: UIButton) {
        
        yearlyImage.image = UIImage(named: "radio_checked_onboarding_premium_icon")
        monthlyImage.image = nil
        continueLabel.text = "Continue"
        planLabel.text = "$9.99 / Month billed annually"
    }
    @IBAction func onMonthly(_ sender: UIButton) {
        
        monthlyImage.image = UIImage(named: "radio_checked_onboarding_premium_icon")
        yearlyImage.image = nil
        continueLabel.text = "Continue"
         planLabel.text = "$9.99 / Month after 1 week"
    }
    
    @IBAction func onPurchase(_ sender: UIButton) {
        
        
    }
    
}
@available(iOS 13.0, *)
extension MainOptionScreen:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainOptionCell", for: indexPath)
        let optionLabel1 = cell.viewWithTag(200) as? UILabel
        let optionLabel2 = cell.viewWithTag(201) as? UILabel
        let optionLabel3 = cell.viewWithTag(202) as? UILabel
        let optionLabel4 = cell.viewWithTag(204) as? UILabel
        
        if indexPath.row == 0 {
            
            optionLabel1?.text = "OCR"
            optionLabel2?.text = "Unlimited Scans"
            optionLabel3?.text = "Password Protection"
            optionLabel4?.text = "No Watermark and Ads"
            
            
        }else if indexPath.row == 1 {
            optionLabel1?.text = "Add Signature"
            optionLabel2?.text = "Cloud Space (20GB)"
            optionLabel3?.text = "Multiple Sharing Option"
            optionLabel4?.text = "Multiple Format Save Option"
            
        }else if indexPath.row == 2 {
            optionLabel1?.text = "HD Scan"
            optionLabel2?.text = "Collage Creator"
            optionLabel3?.text = "Premium Filters"
            optionLabel4?.text = "Add text to documents"
            
        }
        return cell
    }
}
@available(iOS 13.0, *)
extension MainOptionScreen:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let index = mainOptionCollectionView.indexPathForItem(at: center) {
            if index.row == 0{
                pageControllerImageView?.image = UIImage(named: "first_white_page_control")
            }else if index.row == 1{
                pageControllerImageView?.image = UIImage(named: "second_white_page_control")
            }else if index.row == 2{
                pageControllerImageView?.image = UIImage(named: "third_white_page_control")
            }
        }
    }
}
@available(iOS 13.0, *)
extension MainOptionScreen: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: collectionView.frame.size.height)
    }
}
