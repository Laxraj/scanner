//
//  ViewController.swift
//  camscanner
//
//  Created by laxman on 14/08/20.
//  Copyright © 2020 laxman. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ViewController: UIViewController {
 var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
    }

    @objc func update() {
            setNavigation()
      }
    
    func setNavigation(){
        if UserDefaults().dictionary(forKey: USER_DETAILS) != nil && UserDefaults().value(forKey: IS_LOGIN) != nil{
            if UserDefaults().value(forKey: IS_LOGIN) as! String == "1"{
                
//                let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
//                let viewController = storyboard.instantiateViewController(withIdentifier:"RecoverPasswordScreen") as! RecoverPasswordScreen
//                self.navigationController?.pushViewController(viewController, animated: true)
                
                let storyBoard = UIStoryboard(name: "TabBar", bundle: nil)
                let control = storyBoard.instantiateViewController(withIdentifier: "TabBarScreen") as! TabBarScreen
                self.navigationController?.pushViewController(control, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier:"OnboardingScreen") as! OnboardingScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else{
            
            if  UserDefaults().value(forKey: ON_BOARDING) != nil{
                if UserDefaults().value(forKey: ON_BOARDING) as! String == "1" && UserDefaults().value(forKey: ON_BOARDING_PREMIUM) != nil{
                    let storyboard = UIStoryboard(name: "LogIn", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "WelcomeScreen") as! WelcomeScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else if UserDefaults().value(forKey: ON_BOARDING) as! String == "1"{
                    let storyboard = UIStoryboard(name: "MainOption", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier: "MainOptionScreen") as! MainOptionScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                    let viewController = storyboard.instantiateViewController(withIdentifier:"OnboardingScreen") as! OnboardingScreen
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }else{
                let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier:"OnboardingScreen") as! OnboardingScreen
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        }
    }

}

